"""Analyze cross validation results to compare model performance.
"""
from __future__ import annotations

import logging
import os
from itertools import product

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from src.utils import PROCESSED_DATA_DIR, ANALYSIS_DIR

if not os.path.exists(RESULTS_DIR := os.path.join(ANALYSIS_DIR, "cross_validate")):
    os.mkdir(RESULTS_DIR)

logging.basicConfig(
    filename=os.path.join(RESULTS_DIR, "output.txt"), level=logging.INFO
)

if __name__ == "__main__":
    sns.set()
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "cross_val.csv"))
    df["study"] = df.study.map(
        {
            "penn": "Penn-Geisinger flu",
            "walmart": "Walmart flu",
            "exercise": "Exercise",
        }
    )
    df["estimator"] = df.estimator.map(
        {
            "betabinom": "BetaBinom (MLE)",
            "bock": "Bock",
            "conventional": "OLS",
            "james-stein": "James-Stein",
            "nonparametric": "Nonparametric",
            "normal": "Normal (MLE)",
        }
    )
    metrics = {
        "log_likelihood": "Log likelihood",
        "squared_error": "Mean squared error",
        "abs_error": "Mean absolute error",
    }
    df = df.rename(columns=metrics)
    df.groupby(["study", "estimator"])[list(metrics.values())].mean().to_csv(
        os.path.join(RESULTS_DIR, "results.csv")
    )
    for (study, sub_df), metric in product(df.groupby("study"), metrics.values()):
        fig, ax = plt.subplots()
        sns.boxplot(
            data=sub_df, x=metric, y="estimator", orient="h", fliersize=0, ax=ax
        )
        ax.set_ylabel("")
        plt.savefig(
            os.path.join(RESULTS_DIR, f"{study}_{metric}.png"),
            bbox_inches="tight",
            dpi=300,
        )

    # compare normal and nonparametric to James-Stein and Bock estimators
    df_wide = pd.pivot(df, ["study", "sim_no", "fold"], "estimator")
    for estimator0, estimator1, metric in product(
        ("Normal (MLE)", "Nonparametric"), ("Bock", "James-Stein"), metrics.values()
    ):
        logging.info(
            f"How often does {estimator0} have a lower {metric} than {estimator1}"
        )
        df_wide["value"] = df_wide[metric][estimator0] < df_wide[metric][estimator1]
        logging.info(str(df_wide.groupby("study").value.mean()))
