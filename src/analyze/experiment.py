"""Analyze the effort experiment data.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from conditional_inference.bayes import Normal
from conditional_inference.confidence_set import ConfidenceSet

from src.utils import (
    ANALYSIS_DIR,
    ESTIMATES_DIR,
    PROCESSED_DATA_DIR,
    EXPERIMENT_SIMULATIONS_DATAFILE,
    AVERAGE_COLOR,
    RECOMMENDED_COLOR,
)

N_OBS_EXPERIMENT = 1500
N_OBS = 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000
NEEDLE_ARM = "truly best treatment"
if not os.path.exists(RESULTS_DIR := os.path.join(ANALYSIS_DIR, "experiment")):
    os.mkdir(RESULTS_DIR)

if __name__ == "__main__":

    def get_results(n_obs):
        df = experiment_df.iloc[:n_obs]
        ols_results = sm.OLS(df.y, pd.get_dummies(df.arm)).fit(cov_type="HC3")
        index = int(ols_results.params.argmax())
        cs_results = ConfidenceSet.from_results(ols_results).fit()
        return {
            "experiment_identified_best_arm": ols_results.model.exog_names[index]
            == NEEDLE_ARM,
            "experiment_len_ci": np.diff(cs_results.conf_int(columns=index))[0][0],
            "n_obs": n_obs,
        }

    # get experiment results
    sns.set()
    experiment_df = pd.read_csv(
        os.path.join(PROCESSED_DATA_DIR, "effort_experiment.csv")
    ).sort_values("end_time")
    df = pd.DataFrame([get_results(n_obs) for n_obs in N_OBS])

    # plot Bayesian estimates of the experimental data
    fig, ax = plt.subplots(figsize=(10, 10))
    bayes_results = Normal.from_csv(
        os.path.join(ESTIMATES_DIR, "effort_experiment.csv"), sort=True
    ).fit()
    ax = bayes_results.point_plot(
        title="Needle-in-a-haystack results", yname="Points", ax=ax
    )
    ax.axvline(bayes_results.params.max(), color=RECOMMENDED_COLOR)
    ax.axvline(bayes_results.model.prior_mean[0], color=AVERAGE_COLOR)
    plt.savefig(
        os.path.join(RESULTS_DIR, "estimated_effects.png"), bbox_inches="tight", dpi=300
    )

    # get results of simulations of what would happen if we ran this experiment with
    # random assignment
    simulations_df = pd.read_csv(EXPERIMENT_SIMULATIONS_DATAFILE)
    simulations_df["len_ci"] = simulations_df.ppf975 - simulations_df.ppf025
    simulations_df["identified_best_arm"] = simulations_df["rank"] == 0
    simulations_df["len_ci"] = simulations_df.ppf975 - simulations_df.ppf025
    simulations_df = simulations_df[simulations_df.estimator == "Projection"]

    # compute aggregate statistics
    # how likely random assignment is to identify the best arm?
    # how likely is the random assignment CI to be longer than the adaptive assignment CI?
    df = df.merge(simulations_df, on="n_obs")
    df["experiment_ci_shorter"] = df.experiment_len_ci < df.len_ci
    gb = df.groupby(["n_obs", "n_arms"])
    agg_df = (
        gb[["experiment_identified_best_arm", "experiment_len_ci"]].mean().reset_index()
    )
    agg_df["median_len_ci"] = gb.len_ci.quantile(0.5).values
    agg_df["pr_identified_best_arm"] = gb.identified_best_arm.mean().values
    agg_df["pr_experiment_ci_shorter"] = gb.experiment_ci_shorter.mean().values
    agg_df.to_csv(
        os.path.join(RESULTS_DIR, "random_assignment_comparison.csv"), index=False
    )

    # plot probability of identifying the best arm and CI length under random assignment
    agg_df = agg_df.rename(columns={"n_arms": "No. treatments"})
    fig, axs = plt.subplots(2, 1, figsize=(7, 15), sharex=True)

    sns.lineplot(
        data=agg_df,
        x="n_obs",
        y="pr_identified_best_arm",
        hue="No. treatments",
        ax=axs[0],
    )
    axs[0].axvline(N_OBS_EXPERIMENT, linestyle="--")
    axs[0].set_ylabel("Pr. identified best treatment")
    sns.lineplot(
        data=agg_df, x="n_obs", y="median_len_ci", hue="No. treatments", ax=axs[1]
    )
    axs[1].axvline(N_OBS_EXPERIMENT, linestyle="--")
    axs[1].axhline(
        df[df.n_obs == N_OBS_EXPERIMENT].experiment_len_ci.iloc[0], linestyle="--"
    )
    axs[1].set_ylabel("Median length of 95% projection CI")
    axs[1].set_xlabel("Sample size")
    axs[1].get_legend().remove()
    plt.savefig(
        os.path.join(RESULTS_DIR, "random_assignment_comparison.png"),
        bbox_inches="tight",
        dpi=300,
    )
