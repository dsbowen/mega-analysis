"""Simulate a "coin flipping" megastudy.
"""
from __future__ import annotations

import os
import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from conditional_inference.base import ModelBase
from conditional_inference.bayes.improper import Improper
from scipy.stats import bernoulli

from src.utils import ESTIMATES_DIR, PROCESSED_DATA_DIR, ANALYSIS_DIR

if not os.path.exists(RESULTS_DIR := os.path.join(ANALYSIS_DIR, "coin_flip")):
    os.mkdir(RESULTS_DIR)

N_SIMULATIONS = 1000

logging.basicConfig(
    filename=os.path.join(RESULTS_DIR, "output.txt"), filemode="w", level=logging.INFO
)


if __name__ == "__main__":
    np.random.seed(0)
    sns.set()
    fig, axes = plt.subplots(2, 1, figsize=(5, 7), sharex=True)

    # plot results for the Penn-Geisinger flu megastudy
    model = ModelBase.from_csv(os.path.join(ESTIMATES_DIR, "penn.csv"))
    results = Improper(
        model.mean,
        model.cov,
        exog_names=model.exog_names,
        columns=[col for col in model.exog_names if col != "Control"],
    ).fit()
    results.point_plot(ax=axes[0])
    axes[0].set_title("Penn-Geisinger flu megastudy")
    axes[0].set_xlabel("Vaccination rate")
    plt.savefig(os.path.join(RESULTS_DIR, "coin_flip_blank.png"), bbox_inches="tight")

    # simulate the study
    mean_vaccination_rate = (
        results.params.mean()
    )  # mean vaccination rate across treatments
    n_obs = (
        pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "penn.csv"))
        .groupby("arm")
        .y.count()
    )
    n_obs_treatment = n_obs[n_obs.index != "Usual care control"]
    simulation_results = []
    for i in range(N_SIMULATIONS):
        df = pd.concat(
            [
                pd.DataFrame(
                    {
                        "arm": f"x{str(i).zfill(2)}",
                        "target": bernoulli.rvs(mean_vaccination_rate, size=n),
                    }
                )
                for i, n in enumerate(n_obs_treatment)
            ]
        )
        df = pd.concat(
            (
                df,
                pd.DataFrame(
                    {
                        "arm": "Control",
                        "target": bernoulli.rvs(
                            model.mean[model.exog_names == "Control"],
                            size=n_obs["Usual care control"],
                        ),
                    }
                ),
            )
        )
        X = pd.get_dummies(df.arm)
        X.Control = 1
        results = sm.OLS(df.target, X).fit(cov_type="HC3")
        effects = results.params[np.array(results.model.exog_names) != "Control"]
        simulation_results.append(
            {
                "best": effects.max(),
                "average": effects.mean(),
                "control": results.params["Control"],
            }
        )

    # output average estimated effectiveness across simulations
    # and probability that the estimated effectiveness exceeded the Penn Medicine flu study
    simulation_df = pd.DataFrame(simulation_results)
    logging.info("Summary stats for all simulations")
    logging.info(
        f"Average absolute effect compared to control: {simulation_df.best.mean()}"
    )
    logging.info(
        f"Average relative effect compared to control: {((simulation_df.best + simulation_df.control) / simulation_df.control).mean()}"
    )

    # plot and output the results for the last simulation
    df = df[df.arm != "Control"]
    results = sm.OLS(df.target, pd.get_dummies(df.arm)).fit(cov_type="HC3")
    coin_results = Improper.from_results(results, sort=True).fit()
    coin_results.point_plot(ax=axes[1])
    axes[1].set_title("Coin-flipping megastudy")
    axes[1].set_xlabel("Pr. heads")
    plt.savefig(os.path.join(RESULTS_DIR, "coin_flip.png"), bbox_inches="tight")
