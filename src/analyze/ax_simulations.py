"""Analyze the adaptive experimentation simulations.
"""
from __future__ import annotations

import os
from typing import Any

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from src.utils import AX_SIMULATIONS_DATAFILE, ANALYSIS_DIR

if not os.path.exists(RESULTS_DIR := os.path.join(ANALYSIS_DIR, "ax_simulations")):
    os.mkdir(RESULTS_DIR)


def plot_row(
    df: pd.DataFrame,
    y: str,
    ylabel: str,
    plotfile: str = None,
    plotdir: str = RESULTS_DIR,
    sharey: bool = False,
    **relplot_kwargs: Any,
) -> None:
    """Create a ``sns.relplot`` with a single row.

    The columns are the studies.

    Args:
        df (pd.DataFrame): Dataframe with performance metrics.
        y (str): Name of the y variable passed to ``sns.relplot``.
        ylabel (str): y axis label.
        plotfile (str, optional): File to save to plot. If None, the plot will be named
            based on ``y``. Defaults to None.
        plotdir (str, optional): Directory to save the plot. Defaults to RESULTS_DIR.
        sharey (bool, optional): Indicates that all plots share the same y axis.
            Defaults to False.
    """
    g = sns.relplot(
        data=df,
        x="n_obs",
        y=y,
        hue=relplot_kwargs.pop("hue", "strategy"),
        col="study",
        style="strategy",
        markers=True,
        ci=None,
        kind="line",
        facet_kws=dict(sharey=sharey, sharex=False),
        **relplot_kwargs,
    )
    g.set(ylabel=ylabel, xlabel="Sample size")
    plt.savefig(
        os.path.join(plotdir, plotfile or f"{y}.png"), bbox_inches="tight", dpi=300
    )


def plot_grid(
    df: pd.DataFrame,
    y: str,
    ylabel: str,
    plotfile: str = None,
    plotdir: str = RESULTS_DIR,
    sharey: bool = False,
    **relplot_kwargs: Any,
) -> None:
    """Create a ``sns.relplot`` with a grid.

    The columns are the estimators, the rows are the studies.

    Args:
        df (pd.DataFrame): Dataframe with performance metrics.
        y (str): Name of the y variable passed to ``sns.relplot``.
        ylabel (str): y axis label.
        plotfile (str, optional): File to save the plot. If None, the plot will be named
            based on ``y``. Defaults to None.
        plotdir (str, optional): Directory to save the plot. Defaults to RESULTS_DIR.
        sharey (bool, optional): Indicates that all plots on the same row share the same
            y axis. Defaults to False.
    """
    g = sns.relplot(
        data=df,
        x="n_obs",
        y=y,
        hue="strategy",
        row="study",
        col="estimator",
        style="strategy",
        markers=True,
        ci=None,
        kind="line",
        facet_kws=dict(sharey=False, sharex=False),
        **relplot_kwargs,
    )
    g.set(ylabel=ylabel, xlabel="Sample size")
    if sharey:
        for ax_row in g.axes:
            ylim = [ax.get_ylim() for ax in ax_row]
            ymin = min([i[0] for i in ylim])
            ymax = max([i[1] for i in ylim])
            for ax in ax_row:
                ax.set_ylim((ymin, ymax))

    plt.savefig(
        os.path.join(plotdir, plotfile or f"{y}.png"), bbox_inches="tight", dpi=300
    )


def save_relative_improvement_stats(df: pd.DataFrame) -> None:
    """Save a dataframe with relative effectiveness stats.

    The relative effectiveness is how much more effective the adaptive assigner was
    compared with random assignment.

    Args:
        df (pd.DataFrame): Dataframe.
    """
    df.groupby(["n_obs", "study", "strategy"]).improvement.count().to_csv(
        os.path.join(RESULTS_DIR, "counts.csv")
    )
    df = (
        df.groupby(["n_obs", "study", "strategy"])
        .improvement.mean()
        .reset_index()
        .pivot(index=["n_obs", "study"], columns="strategy", values="improvement")
        .reset_index()
    )
    df["pct_improvement_exploration"] = (
        df["Exploration sampling"] / df["Naive random assignment"] - 1
    )
    df["pct_improvement_successive_rejects"] = (
        df["Successive rejects"] / df["Naive random assignment"] - 1
    )
    df = (
        pd.wide_to_long(
            df,
            stubnames="pct_improvement",
            i=["n_obs", "study"],
            j="strategy",
            sep="_",
            suffix="\w+",
        )
        .reset_index()
        .sort_values(["study", "n_obs"])
    )
    df.to_csv(os.path.join(RESULTS_DIR, "improvement.csv"))


def plot_study(df: pd.DataFrame) -> None:
    """Plot key metrics (effetciveness, pr. recommending best treatment, length CI).

    Args:
        df (pd.DataFrame): Dataframe for a single study (e.g., the Penn Medicine study).
    """
    fig, axs = plt.subplots(3, 1, figsize=(7, 15), sharex=True)
    sns.lineplot(
        data=df, x="n_obs", y="improvement", hue="strategy", ci=None, ax=axs[0]
    )
    axs[0].set_ylabel("Effectiveness")
    sns.lineplot(
        data=df, x="n_obs", y="recommended_best_arm", hue="strategy", ci=None, ax=axs[1]
    )
    axs[1].set_ylabel("Pr. recommending best treatment")
    sns.lineplot(
        data=df,
        x="n_obs",
        y="len_ci",
        hue="strategy",
        ci=None,
        estimator=lambda x: np.quantile(x, 0.5),
        ax=axs[2],
    )
    axs[2].set_ylabel("Median length 95% projection CI")
    axs[2].set_xlabel("Sample size")
    for ax in axs[1:]:
        ax.get_legend().remove()
    plt.savefig(
        os.path.join(RESULTS_DIR, f"{df.study.iloc[0]}.png"),
        bbox_inches="tight",
        dpi=300,
    )


if __name__ == "__main__":
    sns.set()
    df = pd.read_csv(AX_SIMULATIONS_DATAFILE)
    df = df[(df.estimator != "Conditional") & (df.selection != "conventional")]
    df = df.sort_values(["study", "n_obs"])
    df["n_obs"] = df.n_obs.astype(str)
    df["study"] = df.study.map(
        {
            "penn": "Penn-Geisinger flu",
            "walmart": "Walmart flu",
            "exercise_min": "Exercise",
        }
    )
    df["strategy"] = df.strategy.map(
        {
            "exploration": "Exploration sampling",
            "successive_rejects": "Successive rejects",
            "random": "Naive random assignment",
        }
    )

    # analyze assignment performance metrics
    df["recommended_best_arm"] = df["rank"] == 0
    df["regret"] = df.best_effect - df.effect
    df["improvement"] = df.effect - df.average_effect

    bandit_df = df.drop_duplicates(["sim_no", "study", "n_obs", "strategy"])
    plot_row(bandit_df, "regret", "Regret")
    plot_row(bandit_df, "improvement", "Improvement over average treatment")
    plot_row(bandit_df, "recommended_best_arm", "Pr. recommended best treatment")
    plot_row(
        bandit_df,
        "rank",
        "Median rank of recommended treatment",
        estimator=lambda x: np.quantile(x, 0.5),
    )
    save_relative_improvement_stats(bandit_df)

    # analyze estimator performance metrics
    df["coverage"] = (df.ppf025 < df.effect) & (df.effect < df.ppf975)
    df["pr_overestimate"] = df.effect < df.point
    df["bias"] = df.point - df.effect
    df["len_ci"] = df.ppf975 - df.ppf025
    df["squared_error"] = (df.effect - df.point) ** 2
    df["abs_error"] = abs(df.effect - df.point)

    plot_grid(df, "coverage", "Pr. true effect in 95% CI", sharey=True)
    plot_grid(df, "pr_overestimate", "Pr. true effect < estimate", sharey=True)
    plot_grid(df, "bias", "Bias", sharey=True)
    plot_grid(
        df,
        "len_ci",
        "Median length 95% CI",
        sharey=True,
        estimator=lambda x: np.quantile(x, 0.5),
    )
    plot_grid(df, "squared_error", "Mean squared error", sharey=True)
    plot_grid(
        df,
        "abs_error",
        "Median absolute error",
        sharey=True,
        estimator=lambda x: np.quantile(x, 0.5),
    )

    # plot the effects of assignment strategies for each study
    df[df.estimator == "Projection"].groupby("study").apply(plot_study)
