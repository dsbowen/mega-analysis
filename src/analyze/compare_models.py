"""Compare OLS, Bayesian, hybrid, and projection models.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from conditional_inference.bayes import Nonparametric

from src.analyzer import make_analyzers
from src.utils import ANALYSIS_DIR, ESTIMATES_DIR

RESULTS_DIRECTORIES = "penn", "walmart", "walmart_multiday", "exercise"

if not os.path.exists(RESULTS_DIR := os.path.join(ANALYSIS_DIR, "compare_models")):
    os.mkdir(RESULTS_DIR)

for path in RESULTS_DIRECTORIES:
    if not os.path.exists(full_path := os.path.join(RESULTS_DIR, path)):
        os.mkdir(full_path)


def plot_walmart_prior() -> None:
    """Plot the prior distribution for the Walmart study."""

    def compute_mean(columns):
        mean, cov = model.mean[columns], model.cov[columns][:, columns]
        ones = np.ones((len(mean), 1))
        cov_inv = np.linalg.inv(cov)
        return np.linalg.inv(ones.T @ cov_inv @ ones) @ ones.T @ cov_inv @ mean

    columns = pd.read_csv(
        filename := os.path.join(ESTIMATES_DIR, "walmart.csv")
    ).columns[1:]
    columns = [col for col in columns if col != "Control"]
    model = Nonparametric.from_csv(filename, columns=columns)
    multiday = np.array(["3 d later" in col for col in model.exog_names])
    prior = model.get_marginal_prior(0)
    x = np.linspace(prior.ppf(0.025), prior.ppf(0.975))
    fig, ax = plt.subplots()
    sns.lineplot(x=x, y=prior.pdf(x), ax=ax)
    ax.axvline(
        compute_mean(multiday),
        linestyle="--",
        color=sns.color_palette()[1],
        label="Multi day mean",
    )
    ax.axvline(
        compute_mean(~multiday),
        linestyle="--",
        color=sns.color_palette()[2],
        label="Single day mean",
    )
    ax.set_xlabel("Vaccination rate")
    fig.legend()
    fig.savefig(
        os.path.join(RESULTS_DIR, "walmart_prior.png"), bbox_inches="tight", dpi=300
    )


if __name__ == "__main__":
    sns.set()
    np.random.seed(0)
    analyzers = make_analyzers()
    shrinkage_results, bestarm_ci_results = [], []
    for analyzer, path in zip(analyzers, RESULTS_DIRECTORIES):
        full_path = os.path.join(RESULTS_DIR, path)

        # analyze shrinkage
        fig, ax = plt.subplots()
        analyzer.make_bayes_plot(ax=ax)
        plt.savefig(os.path.join(full_path, "bayes.png"), bbox_inches="tight", dpi=300)
        df = analyzer.analyze_shrinkage()
        fig, ax = plt.subplots()
        ax = sns.barplot(data=df, y="model", x="pr_best", orient="h", ax=ax)
        ax.set_ylabel("")
        ax.set_xlabel("Pr. best-performing treatment is truly best")
        plt.savefig(
            os.path.join(full_path, "bestarm_pr_best.png"), bbox_inches="tight", dpi=300
        )
        shrinkage_results.append(df)

        if path == "walmart":
            # create a plot with only the OLS and nonparametric results
            fig, ax = plt.subplots()
            analyzer.make_bayes_plot(ax=ax, models=["OLS", "Nonparametric"])
            plt.savefig(os.path.join(full_path, "bayes_ols_nonparametric.png"), bbox_inches="tight", dpi=300)

        # analyze recommended arm confidence interval
        fig, ax = plt.subplots()
        analyzer.make_recommended_arm_plot(ax=ax)
        plt.savefig(
            os.path.join(full_path, "bestarm_ci.png"), bbox_inches="tight", dpi=300
        )
        bestarm_ci_results.append(analyzer.analyze_recommended_conf_int())

    pd.concat(shrinkage_results).to_csv(
        os.path.join(RESULTS_DIR, "shrinkage.csv"), index=False
    )
    pd.concat(bestarm_ci_results).to_csv(
        os.path.join(RESULTS_DIR, "bestarm_ci.csv"), index=False
    )

    plot_walmart_prior()

