"""Creates an animation to illustrate multi-armed bandit algorithms.
"""
import os

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import statsmodels.api as sm
from scipy.stats import norm, multivariate_normal

from src.utils import FIGURES_DIR

N_USERS = 750
EFFECTS = np.array([0, 0.1, 0.1, 0.2])
COV = np.identity(len(EFFECTS))

if __name__ == "__main__":
    np.random.seed(2357)
    sns.set()
    dist = multivariate_normal(EFFECTS, COV)

    def init():
        del pr_best_records[:]
        del weight_records[:]
        del arm_records[:]
        del n_users[:]
        del arms[:]
        del y[:]
        return effect_plots + pr_best_plots + weight_plots

    def run(i):
        # update data
        if weight_records:
            arm = np.random.choice(np.arange(len(EFFECTS)), p=weight_records[-1])
        else:
            arm = i % len(EFFECTS)
        arms.append(arm)
        y.append(dist.rvs()[arm])
        n_users.append(i)

        # update probability best and weights
        pr_best = weights = np.full(len(EFFECTS), 1 / len(EFFECTS))
        try:
            results = (
                sm.OLS(y, np.identity(len(EFFECTS))[arms]).fit().get_robustcov_results()
            )
            estimated_dist = multivariate_normal(
                results.params, results.cov_params()
            ).rvs(1000)
            pr_best = np.identity(len(EFFECTS))[estimated_dist.argmax(axis=1)].mean(
                axis=0
            )
            weights = pr_best * (1 - pr_best)
            weights /= weights.sum()

            # update the effect plots
            distributions = [
                norm(mean, scale)
                for mean, scale in zip(
                    results.params, np.sqrt(results.cov_params().diagonal())
                )
            ]
            xmin = min([marginal.ppf(0.025) for marginal in distributions])
            xmax = max([marginal.ppf(0.975) for marginal in distributions])
            x = np.linspace(xmin, xmax)
            ymax = max(
                [
                    marginal.pdf(mean)
                    for marginal, mean in zip(distributions, results.params)
                ]
            )
            for plot, marginal in zip(effect_plots, distributions):
                plot.set_data(x, marginal.pdf(x))
            effects_ax.set_xlim((xmin, xmax))
            effects_ax.set_ylim((-0.1, 1.1 * ymax))
        except:
            pass
        pr_best_records.append(pr_best)
        weight_records.append(weights)
        arm_records.extend(len(EFFECTS) * [arm])

        # update the weights and probability best plots
        for i, (pr_best_plot, weight_plot) in enumerate(
            zip(pr_best_plots, weight_plots)
        ):
            pr_best_plot.set_data(n_users, np.array(pr_best_records)[:, i])
            weight_plot.set_data(n_users, np.array(weight_records)[:, i])

        return effect_plots + pr_best_plots + weight_plots

    fig, (effects_ax, pr_best_ax, weights_ax) = plt.subplots(1, 3, figsize=(12, 3))
    effects_ax.set_xlabel("Estimated effect")

    # plot the estiamted probability that each arm is the best
    pr_best_ax.set_ylim(-0.1, 1.1)
    pr_best_ax.set_xlim(-1, N_USERS)
    pr_best_ax.set_xlabel("Sample size")
    pr_best_ax.set_ylabel("Pr. best treatment")

    # plot the assignment weight for each arm
    weights_ax.set_ylim(-0.05, 0.55)
    weights_ax.set_xlim(-1, N_USERS)
    weights_ax.set_xlabel("Sample size")
    weights_ax.set_ylabel("Assignment weight")

    # add line plots for each treatment arm
    effect_plots, pr_best_plots, weight_plots = [], [], []
    for i in range(len(EFFECTS)):
        treatment_name = f"true effect = {EFFECTS[i]}"
        effect_plots.append(effects_ax.plot([], [], label=treatment_name)[0])
        pr_best_plots.append(pr_best_ax.plot([], [], label=treatment_name)[0])
        weight_plots.append(weights_ax.plot([], [], label=treatment_name)[0])
    plt.legend(bbox_to_anchor=(1, 1), loc="upper left")

    pr_best_records = []
    weight_records = []
    arm_records = []
    n_users = []
    arms = []
    y = []

    plt.tight_layout()
    ani = animation.FuncAnimation(
        fig, run, N_USERS, interval=50, init_func=init, blit=True
    )
    ani.save(os.path.join(FIGURES_DIR, "bandit_animation.gif"))
