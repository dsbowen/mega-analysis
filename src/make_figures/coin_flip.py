"""Simulate a "coin flipping" megastudy
"""
from __future__ import annotations

import os
import logging

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from conditional_inference.bayes.classic import LinearClassicBayes
from scipy.stats import bernoulli

from src.utils import (
    ESTIMATES_DIR,
    FIGURES_DIR,
    PROCESSED_DATA_DIR,
    RECOMMENDED_COLOR,
    AVERAGE_COLOR,
)


# number of treatment arms in Penn Medicine study
N_ARMS = 19
# 47306 is the number of participants in Penn Medicine study
# there was also 1 control group
N_OBS = 47306 * N_ARMS / (N_ARMS + 1)
# estimated effectiveness in the Penn Medicine study
OBSERVED_EFFECTIVENESS = 0.025
N_SIMULATIONS = 1
PR_HEADS = 0.441

logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":
    np.random.seed(0)
    sns.set()

    # plot results for the Penn Medicine flu megastudy
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "penn_medicine.csv"))
    df = df[df.control == 0]
    ols_results = sm.OLS(df.y, pd.get_dummies(df.arm)).fit().get_robustcov_results()
    results = LinearClassicBayes.from_results(ols_results, prior_cov=np.inf).fit()
    fig, axs = plt.subplots(2, 1, figsize=(5, 7), sharex=True)
    fig.tight_layout(h_pad=3)
    penn_med_ax = results.point_plot(
        title="Penn Medicine flu megastudy", yname="Pr. vaccinated", ax=axs[0]
    )
    penn_med_ax.axvline(results.params.mean(), color=AVERAGE_COLOR)
    penn_med_ax.axvline(results.params.max(), color=RECOMMENDED_COLOR)
    plt.savefig(os.path.join(FIGURES_DIR, "coin_flip_blank.png"), bbox_inches="tight")

    # simulate the study
    effectiveness = []
    for i in range(N_SIMULATIONS):
        df = pd.concat(
            [
                pd.DataFrame(
                    {
                        "treatment": f"x{str(i).zfill(2)}",
                        "target": bernoulli.rvs(PR_HEADS, size=int(N_OBS / N_ARMS)),
                    }
                )
                for i in range(N_ARMS)
            ]
        )
        results = (
            sm.OLS(df.target, pd.get_dummies(df.treatment))
            .fit()
            .get_robustcov_results()
        )
        effectiveness.append(results.params.max() - results.params.mean())

    # output average estimated effectiveness across simulations
    # and probability that the estimated effectiveness exceeded the Penn Medicine flu study
    effectiveness = np.array(effectiveness)
    logging.info("Summary stats for all simulations")
    logging.info(f"Average effectiveness: {effectiveness.mean()}")
    logging.info(
        f"Pr. effectiveness > {OBSERVED_EFFECTIVENESS}: {(effectiveness > OBSERVED_EFFECTIVENESS).mean()}"
    )

    # plot and output the results for the last simulation

    results = LinearClassicBayes.from_results(results, prior_cov=np.inf).fit(
        cols="sorted"
    )
    coin_flip_ax = results.point_plot(
        title="Coin flip study", yname="Pr. heads", ax=axs[1]
    )
    coin_flip_ax.axvline(results.params.mean(), color=AVERAGE_COLOR)
    coin_flip_ax.axvline(results.params.max(), color=RECOMMENDED_COLOR)
    plt.savefig(os.path.join(FIGURES_DIR, "coin_flip.png"), bbox_inches="tight")
    logging.info("Stats for final simulation")
    logging.info(f"Effect of average treatment: {results.params.mean()}")
    logging.info(f"Effect of recommended treatment: {results.params.max()}")
    logging.info(
        f"Estimated effectiveness: {results.params.max() - results.params.mean()}"
    )
