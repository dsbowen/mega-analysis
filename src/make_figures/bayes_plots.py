"""Create Bayes plots for each megastudy individually.
"""
from __future__ import annotations

import os

import seaborn as sns

from src.analyzer import make_analyzers
from src.utils import FIGURES_DIR

if not os.path.exists(RESULTS_DIR := os.path.join(FIGURES_DIR, "bayes_plots")):
    os.mkdir(RESULTS_DIR)

if __name__ == "__main__":
    sns.set()
    for analyzer in make_analyzers():
        fig = analyzer.make_bayes_plots()
        fig.savefig(
            os.path.join(RESULTS_DIR, f"{analyzer.short_title}.png"),
            bbox_inches="tight",
        )
        fig.axes[1].clear()
        fig.savefig(
            os.path.join(RESULTS_DIR, f"{analyzer.short_title}_blank.png"),
            bbox_inches="tight",
        )
