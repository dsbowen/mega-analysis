"""Plot:

1. Bias of OLS and Bayes after naive random assignment
2. Effectiveness of naive random assignment and exploration sampling
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from src.utils import FIGURES_DIR, AX_SIMULATIONS_DATAFILE, make_blank_figure

if not os.path.exists(
    RESULTS_DIR := os.path.join(FIGURES_DIR, "bias_and_effectiveness")
):
    os.mkdir(RESULTS_DIR)


def make_bias_plots(df: pd.DataFrame, penn_med_df: pd.DataFrame):
    """Create bias plots.

    Args:
        df (pd.DataFrame): Simulation results for previous megastudies.
        penn_med_df (pd.DataFrame): Simulation results for the Penn Medicine flu
            megastudy.
    """

    def get_bias_df(df):
        return df[df.estimator.isin(["OLS", "Bayes"]) & (df.strategy == "random")]

    # create a single plot for Penn Medicine
    _, ax = plt.subplots()
    ax = sns.lineplot(
        data=get_bias_df(penn_med_df),
        x="n_obs",
        y="bias",
        hue="estimator",
        ci=None,
        ax=ax,
    )
    ax.set_xlabel("Sample size")
    ax.set_ylabel("Bias")
    ax.set_title("Penn Medicine flu study")
    ax.axhline(0, linestyle="--")
    plt.savefig(os.path.join(RESULTS_DIR, "bias_penn_med.png"))
    make_blank_figure(ax)
    plt.savefig(os.path.join(RESULTS_DIR, "bias_blank.png"))

    # create a plot for all previous megastudies
    g = sns.relplot(
        data=get_bias_df(df),
        x="n_obs",
        y="bias",
        hue="estimator",
        col="study",
        kind="line",
        ci=None,
        facet_kws={"sharex": False, "sharey": False},
    )
    for ax in g.axes[0]:
        ax.axhline(0, linestyle="--")
    g.set(xlabel="Sample size", ylabel="Bias")
    plt.savefig(os.path.join(RESULTS_DIR, "bias.png"))


def make_effectiveness_plots(df: pd.DataFrame, penn_med_df: pd.DataFrame):
    """Create effectiveness plots.

    Args:
        df (pd.DataFrame): Simulation results for previous megastudies.
        penn_med_df (pd.DataFrame): Simulation results for Penn Medicine flu megastudy.
    """

    def get_effectiveness_df(df):
        return df[df.strategy.isin(["random", "adaptive"])]

    # create a plot for the Penn Medicine flu study only
    _, ax = plt.subplots()
    ax = sns.lineplot(
        data=get_effectiveness_df(penn_med_df),
        x="n_obs",
        y="effectiveness",
        hue="strategy",
        ci=None,
        ax=ax,
    )
    ax.set_xlabel("Sample size")
    ax.set_ylabel("Effectiveness")
    ax.set_title("Penn Medicine flu study")
    plt.savefig(os.path.join(RESULTS_DIR, "effectiveness_penn_med.png"))
    make_blank_figure(ax)
    plt.savefig(os.path.join(RESULTS_DIR, "effectiveness_blank.png"))

    # create a plot for all previous megastudies
    g = sns.relplot(
        data=get_effectiveness_df(df),
        x="n_obs",
        y="effectiveness",
        hue="strategy",
        col="study",
        kind="line",
        ci=None,
        facet_kws={"sharey": False, "sharex": False},
    )
    g.set(ylabel="Effectiveness", xlabel="Sample size")
    plt.savefig(os.path.join(RESULTS_DIR, "effectiveness.png"))


if __name__ == "__main__":
    if not os.path.exists(FIGURES_DIR):
        os.mkdir(FIGURES_DIR)

    sns.set()
    df = pd.read_csv(AX_SIMULATIONS_DATAFILE)
    # focus in Bayesian selection results
    # also remove results from charity simulations with less than 2000 observations
    # estimated distributions are highly skewed for this dataset at small sample sizes
    # and give strange results
    df = df[(df.selection == "bayes") & ((df.study != "charity") | (df.n_obs >= 20000))]

    df["bias"] = df.point - df.effect
    df["effectiveness"] = df.effect - df.average_effect
    df["strategy"] = df.strategy.replace("exploration", "adaptive")
    df["n_obs"] = df.n_obs.astype(str)
    df["study"] = df.study.map(
        {
            "philosophy": "Philosophy",
            "penn_medicine": "Penn medicine flu",
            "walmart": "Walmart flu",
            "charity": "Charity",
            "exercise_min": "Exercise",
        }
    )

    penn_med_df = df[df.study == "Penn medicine flu"]

    make_bias_plots(df, penn_med_df)
    make_effectiveness_plots(df, penn_med_df)
