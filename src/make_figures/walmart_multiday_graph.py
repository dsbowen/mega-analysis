"""Plot OLS estimates for the Walmart flu study, highlighting multi-day treatments.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from conditional_inference.base import ResultsBase
from conditional_inference.bayes.classic import LinearClassicBayes

from src.utils import (
    RAW_DATA_DIR,
    ESTIMATES_DIR,
    FIGURES_DIR,
    RECOMMENDED_COLOR,
    AVERAGE_COLOR,
)

if not os.path.exists(RESULTS_DIR := os.path.join(FIGURES_DIR, "walmart")):
    os.mkdir(RESULTS_DIR)


def set_graph_properties(results: ResultsBase, ax):
    """Set axis parameters (title, axis labels, etc.)

    Args:
        results (ResultsBase): Estimated effects.
        ax (AxesSubplot): Axis to draw on.
    """
    ax.set_title("Walmart flu study")
    ax.set_xlabel("Vaccination rate increase relative to control")
    ax.set_ylabel("Text message")
    ax.axvline(results.params[0], color=RECOMMENDED_COLOR)
    ax.axvline(average_effect, color=AVERAGE_COLOR)


if __name__ == "__main__":
    sns.set()

    # plot OLS estimates with all treatments the same color
    results = LinearClassicBayes.from_csv(
        os.path.join(ESTIMATES_DIR, "walmart.csv"), prior_cov=np.inf
    ).fit()
    average_effect = results.params.mean()
    ax = results.point_plot()
    set_graph_properties(results, ax)
    plt.savefig(os.path.join(RESULTS_DIR, "walmart.png"))

    # plot OLS estimates with treatments colored by multi-day versus single-day
    multiday = pd.read_csv(os.path.join(RAW_DATA_DIR, "walmart.csv")).n_days > 1
    conf_int = results.conf_int(0.05)
    xerr = np.array([results.params - conf_int[:, 0], conf_int[:, 1] - results.params])
    xname = [results.model.exog_names[idx] for idx in results.indices]
    yticks = np.arange(len(xname), 0, -1)
    colors = sns.color_palette()

    fig, ax = plt.subplots()
    for i in range(len(results.params)):
        ax.errorbar(
            x=results.params[i],
            y=yticks[i],
            xerr=np.atleast_2d(xerr[:, i]).T,
            fmt="o",
            color=colors[multiday[i]],
        )
    set_graph_properties(results, ax)
    ax.set_yticks(yticks)
    ax.set_yticklabels(xname)
    plt.savefig(os.path.join(RESULTS_DIR, "walmart_multiday.png"))
