"""Create plots of Bayesian estimates after running a (simulated) adaptive experiment.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
from conditional_inference.bayes.empirical import LinearEmpiricalBayes

from src.utils import (
    PROCESSED_DATA_DIR,
    FIGURES_DIR,
    RECOMMENDED_COLOR,
    AVERAGE_COLOR,
    make_blank_figure,
)
from src.simulator import Simulator

if not os.path.exists(RESULTS_DIR := os.path.join(FIGURES_DIR, "ax_example")):
    os.mkdir(RESULTS_DIR)

N_OBS = 50000
BATCH_SIZE = 100


def run_simulation(
    datafile: str,
    n_obs: int,
    batch_size: int,
    title: str,
    yname: str,
    ylabel: str,
    ax=None,
):
    """Run an adaptive simulation.

    Args:
        datafile (str): Name of the datafile used to run the simulation.
        n_obs (int): Number of observations (sample size).
        batch_size (int): Batch size for exploration sampling.
        title (str): Graph title.
        yname (str): Dependent variable name.
        ylabel (str): Name of treatment arms.
        ax (AxesSubplot, optional): Axis to draw on. Defaults to None.

    Returns:
        AxesSubplot: Axis with Bayesian estimates point plot.
    """
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, datafile))
    df = df[df.arm != "control"]
    simulator = Simulator(df, n_obs)
    simulator.run_exploration_sampling(batch_size)
    ols_results = (
        sm.OLS(simulator.y_simulated, simulator.X_simulated)
        .fit()
        .get_robustcov_results()
    )
    bayes_model = LinearEmpiricalBayes.from_results(ols_results)
    params = bayes_model.fit().params
    ax = bayes_model.fit(cols=(-params).argsort()).point_plot(
        title=title, yname=yname, ax=ax
    )
    ax.set_ylabel(ylabel)
    ax.axvline(bayes_model.estimate_prior_params()[0], color=AVERAGE_COLOR)
    ax.axvline(params.max(), color=RECOMMENDED_COLOR)

    return ax


if __name__ == "__main__":
    np.random.seed(0)
    sns.set()
    ax = run_simulation(
        "penn_medicine.csv",
        50000,
        100,
        "Penn Medicine adaptive simulation",
        "Vaccination rate",
        "Text message",
    )
    plt.savefig(os.path.join(RESULTS_DIR, "ax_example.png"))
    make_blank_figure(ax)
    plt.savefig(os.path.join(RESULTS_DIR, "ax_example_blank.png"))
