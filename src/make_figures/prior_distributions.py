"""Plot prior distributions for existing megastudies.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from conditional_inference.bayes.empirical import LinearEmpiricalBayes
from scipy.stats import norm

from src.utils import ESTIMATES_DIR, FIGURES_DIR

if not os.path.exists(RESULTS_DIR := os.path.join(FIGURES_DIR, "prior_plots")):
    os.mkdir(RESULTS_DIR)


def make_prior_plot(datafile: str, title: str, yname: str, plotfile: str):
    """Plot the prior distribution.

    Args:
        datafile (str): Datafile to analyze.
        title (str): Plot title.
        yname (str): Name of the dependent variable.
        plotfile (str): Name of the plot file.
    """
    model = LinearEmpiricalBayes.from_csv(os.path.join(ESTIMATES_DIR, datafile))
    prior_params = model.estimate_prior_params()
    dist = norm(prior_params[0][0], np.sqrt(prior_params[1]))
    x = np.linspace(dist.ppf(0.025), dist.ppf(0.975))
    ax = sns.lineplot(x=x, y=dist.pdf(x))
    ax.set_title(title)
    ax.set_xlabel(yname)
    plt.savefig(os.path.join(RESULTS_DIR, plotfile))


if __name__ == "__main__":
    sns.set()

    make_prior_plot(
        "penn_medicine.csv",
        "Prior for Penn Medicine flu study",
        "Vaccination rate increase relative to control",
        "penn_medicine.png",
    )
