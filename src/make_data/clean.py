from __future__ import annotations

import os

import numpy as np
import pandas as pd

from src.utils import RAW_DATA_DIR, EXTERNAL_DATA_DIR, PROCESSED_DATA_DIR, CONTROL_ARM

# assume participants in the effort experiment who scored higher than this cheated
# e.g., by using an automated key-pressing program
MAX_TARGET = 4000


def reconstruct_penn() -> None:
    def make_treatment_df(row):
        return pd.DataFrame(
            {
                "y": row.Vaccinated * [1] + row.Unvaccinated * [0],
                "arm": row.name,
                "multiple_days": row.multiple_days,
                "control": int(row.name == "Usual care control"),
            }
        )

    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "penn", "Aggregate Data.csv"))
    df = df.groupby("Experimental Condition")[
        ["Total Participants", "Vaccinated"]
    ].sum()
    df["Unvaccinated"] = df["Total Participants"] - df.Vaccinated
    df["multiple_days"] = (
        df.index.str.contains("3 d") | df.index.str.contains("72 h")
    ).astype(int)
    data = pd.concat(list(df.apply(make_treatment_df, axis=1))).reset_index(drop=True)
    data.index.name = "participant_id"
    data.to_csv(os.path.join(PROCESSED_DATA_DIR, "penn.csv"))


def reconstruct_walmart() -> None:
    def make_treatment_df(row):
        return pd.DataFrame(
            {
                "y": int(row.n_vaccinated) * [1] + int(row.N - row.n_vaccinated) * [0],
                "arm": row.name,
                "multiple_days": int("3 d later" in row.name),
                "control": int(row.name == "Control"),
            }
        )

    # approximately reconstruct the Walmart flu megastudy dataset
    pct_vaccinated_control = 0.294  # from the PNAS publication
    gist_df = pd.read_csv(
        os.path.join(
            EXTERNAL_DATA_DIR, "walmart/WMTAttributes_Analysis_Data1and3mo.csv"
        )
    )
    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "walmart/dec_estsmat_js.csv"))
    df.term = df.term.str[len("condition_name") :].str.lstrip("0")
    df = df.merge(gist_df, left_on="term", right_on="Intervention")
    df = df.set_index("Gist")[["N", "estimate"]]
    df.estimate += pct_vaccinated_control
    # 689693 participants in total
    df = pd.concat(
        [
            df,
            pd.DataFrame(
                {"N": [689693 - df.N.sum()], "estimate": [pct_vaccinated_control]},
                index=["Control"],
            ),
        ]
    )
    df["n_vaccinated"] = round(df.N * df.estimate)
    df = pd.concat(list(df.apply(make_treatment_df, axis=1))).reset_index(drop=True)
    df.index.name = "participant_id"
    df.to_csv(os.path.join(PROCESSED_DATA_DIR, "walmart.csv"))


def clean_exercise() -> None:
    """Clean exercise dataset."""
    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "pptdata.csv"))
    df = df.rename(columns={"Lc": "cohort_weeks", "Ngc": "cohort_size", "visits": "y"})
    # keep data from pre-intervention and intervention period
    # after weeks 4 is the post-intervention period
    df = df[(-52 <= df.week) & (df.week <= 4)]
    df["n_weeks"] = df.groupby("participant_id").week.transform("count")
    # indicates this observation is in the intervention period
    df["intervention_period"] = (1 <= df.week) & (df.week <= 4)
    df["arm"] = df.exp_condition.replace("Placebo Control", CONTROL_ARM)
    df["cohort_week"] = pd.Categorical(df["cohort"] + "_" + df["week"].astype(str))
    df["cohort_week"] = df.cohort_week.cat.codes
    df[
        [
            "y",
            "participant_id",
            "cohort_week",
            "cohort_weeks",
            "arm",
            "intervention_period",
            "n_weeks",
            "cohort_size",
        ]
    ].to_csv(os.path.join(PROCESSED_DATA_DIR, "exercise.csv"), index=False)

    # create a smaller dataset where each participant is associated with one observation
    # and "y" is the increase in average weekly gym visits during the intervention
    # period relative to the pre-intervention period
    # this allows for faster computation during simulations
    y_mean = (
        df.groupby(["participant_id", "intervention_period"]).y.mean().reset_index()
    )
    y_mean["intervention_period"] = y_mean.intervention_period.map(
        {True: "intervention", False: "pre_intervention"}
    )
    y_mean = y_mean.pivot(
        index="participant_id", columns="intervention_period", values="y"
    )
    y_mean["y"] = y_mean.intervention - y_mean.pre_intervention
    collapsed_df = (
        df.drop_duplicates("participant_id")[["participant_id", "arm"]]
        .merge(y_mean, on="participant_id")
        .dropna()
    )
    collapsed_df.to_csv(
        os.path.join(PROCESSED_DATA_DIR, "exercise_min.csv"), index=False
    )


def clean_effort_experiment() -> None:
    """Clean data from the adaptive effort experiment."""
    df = pd.read_csv(os.path.join(RAW_DATA_DIR, "effort_experiment.csv")).rename(
        columns={"target": "y"}
    )
    df = df.dropna(subset=["treatment_index", "y"])
    df = df[df.y < MAX_TARGET]
    n_arms = len(df.treatment_index.unique())
    df["arm"] = df.treatment_index.map(
        {
            i: f"haystack_{str(i).zfill(2)}" if i > 0 else "truly best treatment"
            for i in np.arange(n_arms)
        }
    )
    df.to_csv(os.path.join(PROCESSED_DATA_DIR, "effort_experiment.csv"), index=False)


if __name__ == "__main__":
    reconstruct_penn()
    reconstruct_walmart()
    clean_exercise()
    clean_effort_experiment()
