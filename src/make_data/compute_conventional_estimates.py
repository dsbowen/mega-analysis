from __future__ import annotations

import os

import numpy as np
import pandas as pd
import statsmodels.api as sm
from conditional_inference.base import ModelBase
from linearmodels.panel.model import PanelOLS

from src.utils import CONTROL_ARM, ESTIMATES_DIR, EXTERNAL_DATA_DIR, PROCESSED_DATA_DIR


def estimate_ols_params(df: pd.DataFrame) -> tuple[np.ndarray, np.ndarray]:
    """Compute OLS estimates.

    Args:
        df (pd.DataFrame): Dataframe with which to estimate treatment effects.

    Returns:
        tuple[np.array, np.array]: (# params,) mean vector, (# params, # params) covariance
            matrix.
    """
    X = pd.get_dummies(df.arm)
    if CONTROL_ARM in X:
        X[CONTROL_ARM] = 1
    results = sm.OLS(df.y, X).fit(cov_type="HC3")
    return results.params, results.cov_params().values


def estimate_flu_params(df) -> tuple[np.ndarray, np.ndarray]:
    """Compute conventional estimates for the flu studes.

    Args:
        df (pd.DataFrame): Dataframe with which to estimate treatment effects.

    Returns:
        tuple[np.array, np.array]: (# params,) mean vector, (# params, # params) covariance
            matrix.
    """
    df = df.set_index("arm")
    return df["mean"], np.diag(df["mean"] * (1 - df["mean"]) / df.n_obs)


def clean_penn_estimates() -> tuple[np.ndarray, np.ndarray]:
    agg_df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "penn", "Aggregate Data.csv"))
    agg_df = agg_df[agg_df["Experimental Condition"] == "Usual care control"][
        ["Total Participants", "Vaccinated"]
    ].sum()
    control_vaccination_rate = agg_df.Vaccinated / agg_df["Total Participants"]

    df = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "penn", "final-results.csv"))
    # assume control variance is approximately equal to the variance of the other conditions
    control_variance = 0.5 * (df.SE ** 2).mean()
    # convert standard errors to the standard error of the vaccination rate, not the standard error of the effect estimate
    df["variance"] = df.SE ** 2 - control_variance
    df.Estimate += control_vaccination_rate
    df = pd.concat(
        (
            df,
            pd.DataFrame(
                {
                    "condition": ["Control"],
                    "Estimate": [control_vaccination_rate],
                    "variance": [control_variance],
                }
            ),
        )
    )
    df = df.set_index("condition")
    return df.Estimate, np.diag(df.variance)


def clean_walmart_estimates() -> tuple[np.ndarray, np.ndarray]:
    pct_vaccinated_control = 0.294  # from the PNAS publication
    gist_df = pd.read_csv(
        os.path.join(
            EXTERNAL_DATA_DIR, "walmart/WMTAttributes_Analysis_Data1and3mo.csv"
        )
    )

    # means are the estimated effects; need to add control vaccination rate
    mean = pd.read_csv(os.path.join(EXTERNAL_DATA_DIR, "walmart/dec_estsmat_js.csv"))
    mean.term = mean.term.str[len("condition_name") :].str.lstrip("0")
    mean = mean.merge(gist_df, left_on="term", right_on="Intervention")
    mean = mean.set_index("Gist").estimate
    mean += pct_vaccinated_control
    mean = pd.concat([mean, pd.Series([pct_vaccinated_control], index=["Control"])])

    # similarly, need to adjust covariance matrix to reflect vaccination rates
    cov = pd.read_csv(
        os.path.join(EXTERNAL_DATA_DIR, "walmart/dec_vcov_js.csv")
    ).values[:, 1:]
    var_control = cov[0, 1]
    cov = np.diag(cov.diagonal() - cov[0, 1])
    cov = np.vstack(
        [np.hstack([cov, np.zeros((len(cov), 1))]), np.zeros((1, len(cov) + 1))]
    )
    cov[-1, -1] = var_control
    return mean, cov


def estimate_exercise_params(df) -> tuple[np.ndarray, np.ndarray]:
    """Compute conventional estimates for exercise study.

    Args:
        df (pd.DataFrame): Dataframe with which to estimate treatment effects.

    Returns:
        tuple[np.array, np.array]: (# params,) mean vector, (# params, # params) covariance
            matrix.
    """
    df = df.set_index(["participant_id", "cohort_week"])

    # get exogenous variables
    treatment_dummies = pd.get_dummies(df.arm)
    X = treatment_dummies.apply(lambda x: x * df.intervention_period)
    X[CONTROL_ARM] = 1

    # fit model
    model = PanelOLS(
        df.y,
        X,
        weights=df.cohort_weeks / (df.n_weeks * df.cohort_size),
        entity_effects=True,
        time_effects=True,
    )
    results = model.fit(cov_type="clustered", cluster_entity=True, cluster_time=True)
    return results.params, results.cov.values


def to_csv(mean: np.array, cov: np.array, filename: str) -> None:
    """Save results to csv.

    Args:
        mean (np.array): (# params,) vector of estimated means.
        cov (np.array): (# params, # params) estimated covariance matrix.
        filename (str): File name to save the results to.
    """
    ModelBase(mean, cov, sort=True).to_csv(os.path.join(ESTIMATES_DIR, filename))


if __name__ == "__main__":
    to_csv(*clean_penn_estimates(), "penn.csv")
    to_csv(*clean_walmart_estimates(), "walmart.csv")

    mean, cov = estimate_exercise_params(
        pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "exercise.csv"))
    )
    to_csv(mean, cov, "exercise.csv")
    mean, cov = estimate_ols_params(
        pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "effort_experiment.csv"))
    )
    to_csv(mean, cov, "effort_experiment.csv")
