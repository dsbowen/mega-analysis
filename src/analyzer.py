"""Convenience class for post-selection inference.
"""
from __future__ import annotations

import os
from typing import Any, Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from conditional_inference.base import ModelBase
from conditional_inference.bayes import Improper, Normal, Nonparametric
from conditional_inference.bayes.base import BayesBase
from conditional_inference.confidence_set import ConfidenceSet
from conditional_inference.rank_condition import RankCondition
from scipy.optimize import minimize
from scipy.stats import beta, binom

from src.utils import ESTIMATES_DIR, AVERAGE_COLOR


def make_analyzers() -> list[Analyzer]:
    """Create analyzers for all megastudies.

    Returns:
        list[Analyzer]: Analyzers for each megastudy.
    """
    model = ModelBase.from_csv(os.path.join(ESTIMATES_DIR, "penn.csv"))
    penn_columns = [col for col in model.exog_names if col != "Control"]
    model = ModelBase.from_csv(os.path.join(ESTIMATES_DIR, "walmart.csv"))
    walmart_columns = [col for col in model.exog_names if col != "Control"]
    return [
        Analyzer(
            "penn.csv",
            "Penn-Geisinger flu study",
            binary_outcome=True,
            xlabel="Vaccination rate",
            ylabel="Text message treatment",
            columns=penn_columns,
        ),
        Analyzer(
            "walmart.csv",
            "Walmart flu study",
            binary_outcome=True,
            xlabel="Vaccination rate",
            ylabel="Text message treatment",
            columns=walmart_columns,
        ),
        Analyzer(
            "walmart.csv",
            "Walmart flu study multi-day treatments",
            binary_outcome=True,
            xlabel="Vaccination rate",
            ylabel="Text message treatment",
            columns=[col for col in walmart_columns if "3 d later" in col],
        ),
        Analyzer(
            "exercise.csv",
            "Exercise study",
            xlabel="Increase in weekly gym visits compared to control",
            ylabel="Intervention",
        ),
    ]


class BetaBinomial(BayesBase):
    """Empirical Bayes estimator for binomial distributed variables with a beta prior."""

    def __init__(self, *args, **kwargs):
        def neg_log_likelihood(params):
            pdf = beta.pdf(x, *params)
            return -sum(
                [
                    np.log((np.array([binom.pmf(m, n, p) for p in x]) * pdf).sum())
                    for m, n in zip(self.n_successes, self.n_observations)
                ]
            )

        super().__init__(*args, **kwargs)
        self.n_observations = (
            self.mean * (1 - self.mean) / self.cov.diagonal()
        ).round()
        self.n_successes = (self.mean * self.n_observations).round()
        total_obs = self.n_observations.sum()
        std = self.mean.std()
        x = np.linspace(
            max(self.mean.min() - 2 * std, 0),
            min(self.mean.max() + 2 * std, 1),
            num=200,
        )
        self.params = minimize(
            neg_log_likelihood, [1, 1], bounds=[(1, 2 * total_obs), (1, 2 * total_obs)]
        ).x

    def _get_marginal_prior(self, index):
        return beta(*self.params)

    def _get_marginal_distribution(self, index):
        return beta(
            self.params[0] + self.n_successes[index],
            self.params[1] + self.n_observations[index] - self.n_successes[index],
        )


class Analyzer:
    """Convenience class for Bayesian and post-selection analysis.

    Args:
        datafile (str): Name of the data file.
        short_title (str): Short title.
        title (str): Plot title.
        ylabel (str): y-axis label.
        endog_names (str): Name of the endogenous variable.
        **kwargs (Any): Passed to model constructors.
    """

    def __init__(
        self,
        datafile: str,
        title: str,
        xlabel: str = None,
        ylabel: str = None,
        binary_outcome: bool = False,
        **kwargs: Any,
    ):
        datafile = os.path.join(ESTIMATES_DIR, datafile)
        # fit OLS, Bayes, confidence set, and hybrid models
        kwargs.update({"sort": True, "endog_names": xlabel})
        self.results = {
            "OLS": Improper.from_csv(datafile, **kwargs).fit(),
            "Normal (MLE)": Normal.from_csv(datafile, **kwargs).fit(),
            "James-Stein": Normal.from_csv(
                datafile, fit_method="james_stein", **kwargs
            ).fit(),
            "Bock": Normal.from_csv(datafile, fit_method="bock", **kwargs).fit(),
            "Nonparametric": Nonparametric.from_csv(datafile, **kwargs).fit(),
            "Hybrid": RankCondition.from_csv(datafile, **kwargs).fit(beta=0.005),
            "Projection": ConfidenceSet.from_csv(datafile, **kwargs).fit(),
        }
        if binary_outcome:
            self.results["BetaBinom (MLE)"] = BetaBinomial.from_csv(
                datafile, **kwargs
            ).fit()
        self.title = title
        self.xlabel = xlabel
        self.ylabel = ylabel

    def get_bayesian_results(self) -> dict[str, BayesBase]:
        """Get the results of the Bayesian estimators.

        Returns:
            dict[str, BayesBase]: Maps the model name to the result.
        """
        models = ["OLS", "Normal (MLE)", "James-Stein", "Bock", "Nonparametric"]
        if "BetaBinom (MLE)" in self.results:
            models.append("BetaBinom (MLE)")

        return {model: self.results[model] for model in models}

    def analyze_shrinkage(self) -> pd.DataFrame:
        """Compute standard deviations and average shrinkage.

        Returns:
            pd.DataFrame: Results.
        """

        def compute_shrinkage(model, result):
            if model == "OLS":
                return 0

            prior_mean = result.model.get_marginal_prior(0).mean()
            return (
                1
                - abs(results[model].params - prior_mean).mean()
                / abs(results["OLS"].params - prior_mean).mean()
            )

        results = self.get_bayesian_results()
        return pd.DataFrame(
            [
                {
                    "study": self.title,
                    "model": model,
                    "std": result._posterior_rvs.std(axis=1).mean(),
                    "shrinkage": compute_shrinkage(model, result),
                    "pr_best": result.rank_df.values[0, 0],
                }
                for model, result in results.items()
            ]
        )

    def make_bayes_plot(self, ax=None, models=None) -> Optional[plt.figure.Figure]:
        """Make Bayesian analysis plots.

        Args:
            ax (AxesSubplot, optional): Axis to draw on. Defaults to None.
            models (list, optional): List of models to plot. Defaults to None.

        Returns:
            Optional[plt.figure.Figure]: If no axes were given, this return the newly
                created figure.
        """
        results = self.get_bayesian_results()
        if models is not None:
            results = {key: value for key, value in results.items() if key in models}
        yticks = -np.arange(self.results["OLS"].model.n_params).astype(int)
        df = pd.concat(
            [
                pd.DataFrame(
                    {"model": model, "estimates": result.params, "treatment": yticks}
                )
                for model, result in results.items()
            ]
        )
        ax = sns.scatterplot(
            data=df, x="estimates", y="treatment", hue="model", style="model", ax=ax
        )
        ax.set_xlabel(self.xlabel)
        ax.set_ylabel(self.ylabel)
        ax.set_yticks(yticks)
        ax.set_yticklabels(self.results["OLS"].model.exog_names)
        return ax

    def analyze_recommended_conf_int(self) -> pd.DataFrame:
        """Get the 95% CIs for the best-performing treatment.

        Returns:
            pd.DataFrame: Results.
        """
        df = pd.DataFrame(
            [
                {
                    "study": self.title,
                    "model": model,
                    "confint": result.conf_int(columns=0),
                }
                for model, result in self.results.items()
            ]
        )
        df["average"] = self.results["OLS"].params.mean()
        return df

    def make_recommended_arm_plot(self, ax=None) -> Optional[plt.figure.Figure]:
        """Plot OLS, hybrid, and projection analysis for top-performing arm.

        Args:
            ax (AxesSubplot, optional): Axis to draw on. Defaults to None.

        Returns:
            Optional[plt.figure.Figure]: If ``ax`` was None, return the newly created
                figure.
        """
        # collect point estimates (params) and error bar parameters based on the 95% CI
        models, params, err = ("OLS", "Hybrid", "Projection"), [], []
        for model in models:
            results = self.results[model]
            param = results.params[0]
            conf_int = results.conf_int()[0]
            params.append(param)
            err.append([param - conf_int[0], conf_int[1] - param])

        # create and save plot
        ax.set_title(self.title)
        yticks = -np.arange(len(params))
        ax.errorbar(params, yticks, xerr=np.array(err).T, fmt="o")
        ax.set_yticks(yticks)
        ax.set_yticklabels(models)
        ax.axvline(self.results["OLS"].params.mean(), color=AVERAGE_COLOR)
        ax.set_xlabel(self.xlabel)

        return ax
