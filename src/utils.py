"""Global variables and utilities.
"""
from __future__ import annotations

import os

import matplotlib.pyplot as plt

# paths to data files
DATA_DIR = "data"
RAW_DATA_DIR = os.path.join(DATA_DIR, "raw")
EXTERNAL_DATA_DIR = os.path.join(DATA_DIR, "external")
PROCESSED_DATA_DIR = os.path.join(DATA_DIR, "processed")
ESTIMATES_DIR = os.path.join(DATA_DIR, "conventional_estimates")

# paths to simulation data
AX_SIMULATIONS_DATAFILE = os.path.join(PROCESSED_DATA_DIR, "ax_simulations.csv")
EXPERIMENT_SIMULATIONS_DATAFILE = os.path.join(
    PROCESSED_DATA_DIR, "experiment_simulations.csv"
)

# paths to results directories
if not os.path.exists(RESULTS_DIR := "results"):
    os.mkdir(RESULTS_DIR)
if not os.path.exists(ANALYSIS_DIR := os.path.join(RESULTS_DIR, "analysis")):
    os.mkdir(ANALYSIS_DIR)
if not os.path.exists(FIGURES_DIR := os.path.join(RESULTS_DIR, "figures")):
    os.mkdir(FIGURES_DIR)

# constants
CONTROL_ARM = "Control"
RECOMMENDED_COLOR = "#4dac26"
AVERAGE_COLOR = "#d01c8b"


def make_blank_figure(ax):
    """Create a blank axis with the same axes as ``ax``.

    Args:
        ax (AxesSubplot): Axis used to create the blank figure.

    Returns:
        AxesSubplot: Blank axis.
    """
    _, blank_ax = plt.subplots()
    blank_ax.set_xlim(ax.get_xlim())
    blank_ax.set_xticklabels(ax.get_xticklabels())
    blank_ax.set_xticks(ax.get_xticks())
    blank_ax.set_xlabel(ax.get_xlabel())
    blank_ax.set_ylim(ax.get_ylim())
    blank_ax.set_yticklabels(ax.get_yticklabels())
    blank_ax.set_yticks(ax.get_yticks())
    blank_ax.set_title(ax.get_title())
    blank_ax.set_ylabel(ax.get_ylabel())
    return blank_ax
