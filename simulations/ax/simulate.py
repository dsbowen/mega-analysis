"""Simulate the megastudies using adaptive random assignment.
"""
from __future__ import annotations

import logging
import os
import sys

import numpy as np
import pandas as pd

from src.simulator import Simulator
from src.utils import PROCESSED_DATA_DIR, CONTROL_ARM

# directory to store results
RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "results")
# number of observations to simulate
N_OBS = 5000, 10000, 20000, 50000, 100000
# batch size for exploration sampling
BATCH_SIZE = 100

logging.basicConfig(level=logging.INFO)


if __name__ == "__main__":
    sim_no, datafile = sys.argv[1:]
    logging.info(f"Running simulation {sim_no} with datafile {datafile}.")
    sim_no = int(sim_no)
    np.random.seed(sim_no)

    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, datafile))
    simulator = Simulator(df[df.arm != CONTROL_ARM], N_OBS)
    simulator.run_random_assignment()
    simulator.run_successive_rejects()
    simulator.run_exploration_sampling(BATCH_SIZE)
    simulator.results["sim_no"] = sim_no
    datafile_stub = datafile[: -len(".csv")]
    simulator.results["study"] = datafile_stub
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    simulator.results.to_csv(
        os.path.join(RESULTS_DIR, f"sim_no={sim_no}-datafile={datafile_stub}.csv"),
        index=False,
    )
