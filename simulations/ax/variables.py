from itertools import product

N_SIMULATIONS = 1000
DATAFILE = "exercise_min.csv", "penn.csv", "walmart.csv"

variables = product(range(N_SIMULATIONS), DATAFILE)
