"""Simulate what would have happened in the effort study using naive random assignment.
"""
from __future__ import annotations

import os
import sys

import numpy as np
import pandas as pd

from src.simulator import Simulator
from src.utils import PROCESSED_DATA_DIR

# directory to store results
RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "results")
NEEDLE_ARM = "truly best treatment"

# simulation parameters
N_OBS = 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000
N_ARMS = 10, 20, 30, 40, 50, 60

if __name__ == "__main__":

    def make_hay_df(i):
        hay_df = df[~needle_mask]
        hay_df["arm"] = f"haystack_{str(i).zfill(2)}"
        return hay_df

    sim_no = int(sys.argv[1])
    np.random.seed(sim_no)
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, "effort_experiment.csv"))
    needle_mask = df.arm == NEEDLE_ARM

    # run simulations for various numbers of arms and observations
    results = []
    for n_arms in N_ARMS:
        temp_df = pd.concat(
            [df[needle_mask]] + [make_hay_df(i) for i in range(n_arms - 1)],
            ignore_index=True,
        )
        simulator = Simulator(
            temp_df, N_OBS, use_bootstrap=False, use_bayesian_weighting=False
        )
        simulator.run_random_assignment()
        simulator.results["n_arms"] = n_arms
        results.append(simulator.results)

    # store results
    results_df = pd.concat(results)
    results_df["sim_no"] = sim_no
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    results_df.to_csv(os.path.join(RESULTS_DIR, f"sim_no={sim_no}.csv"), index=False)
