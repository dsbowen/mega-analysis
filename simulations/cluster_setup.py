import importlib
import os
import sys

FILE_DIR = os.path.dirname(os.path.abspath(__file__))
VARIABLES_TXT = os.path.join(FILE_DIR, "variables.txt")
ROOT_DIR_TXT = os.path.join(FILE_DIR, "root.txt")
OUTPUT_DIR = "output"
RESULTS_DIR = "results"
VARIABLES_IMPORT = "variables"


if __name__ == "__main__":
    def get_line(args):
        try:
            return f"{' '.join([str(arg) for arg in args])}\n"
        except TypeError:
            # args is not iterable
            return f"{args}\n"

    # get the directory containing the simulations
    if not os.path.exists(root_dir := sys.argv[1].rstrip("/")):
        raise ValueError(f"Directory {root_dir} does not exist.")

    # write variables to file
    variables = importlib.import_module(f".{VARIABLES_IMPORT}", package=root_dir.replace("/", "."))
    with open(VARIABLES_TXT, "w") as f:
        f.writelines([get_line(args) for args in variables.variables])

    # store root directory
    with open(ROOT_DIR_TXT, "w") as f:
        f.write(root_dir)

    # create directory for simulation results
    if not os.path.exists(results_dir := os.path.join(root_dir, RESULTS_DIR)):
        os.mkdir(results_dir)

    # create directory for cluster output
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    for root, dirs, files in os.walk(OUTPUT_DIR):
        for file in files:
            os.remove(os.path.join(root, file))
    