import glob
import logging
import os
import sys

import pandas as pd

from cluster_setup import OUTPUT_DIR, ROOT_DIR_TXT, RESULTS_DIR
from src.utils import PROCESSED_DATA_DIR

if __name__ == "__main__":
    # get the directory containing the simulations
    if not os.path.exists(root_dir := open(ROOT_DIR_TXT, "r").read()):
        raise ValueError(f"Directory {root_dir} does not exist.")

    # concatenate and store the results
    dfs = []
    results_path = os.path.join(root_dir, RESULTS_DIR)
    for path in glob.glob(os.path.join(results_path, "*.csv")):
        try:
            dfs.append(pd.read_csv(path))
        except:
            logging.warning(f"Error with file {path}")
    pd.concat(dfs).to_csv(os.path.join(PROCESSED_DATA_DIR, sys.argv[1]), index=False)
