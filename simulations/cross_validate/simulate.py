"""Perform cross validation.
"""
from __future__ import annotations

import logging
import os
import sys
from typing import Any, Type

import numpy as np
import pandas as pd
from conditional_inference.bayes import Improper, Nonparametric, Normal
from conditional_inference.bayes.base import BayesBase
from sklearn.model_selection import StratifiedKFold

from src.analyzer import BetaBinomial
from src.make_data.compute_conventional_estimates import (
    estimate_ols_params,
    estimate_exercise_params,
)
from src.utils import PROCESSED_DATA_DIR, CONTROL_ARM

# directory to store results
RESULTS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), "results")

logging.basicConfig(level=logging.INFO)

COMPUTE_ESTIMATES = {
    "penn.csv": estimate_ols_params,
    "walmart.csv": estimate_ols_params,
    "exercise.csv": estimate_exercise_params,
}


class Estimator:
    """Stores cross validation results for a Bayesian estimator.

    Args:
        model_cls (Type[BayesBase]): Type of Bayesian estimator to use.
    """

    def __init__(self, model_cls: Type[BayesBase], **kwargs: Any):
        self.model_cls = model_cls
        self.kwargs = kwargs
        self.log_likelihood = []
        self.squared_error = []
        self.abs_error = []

    def update(
        self,
        train_mean: np.ndarray,
        train_cov: np.ndarray,
        test_mean: np.ndarray,
        test_cov: np.ndarray,
    ) -> None:
        """Update results using the training and test data for a particular fold.

        Args:
            train_mean (np.ndarray): (# params,) array of conventionally estimated means
                for the training data.
            train_cov (np.ndarray): (# params, # params) covariance matrix.
            test_mean (np.ndarray): (# params,) array of conventionally estimated means
                for the test data.
            test_cov (np.ndarray): (# params, # params) covariance matrix.
        """
        results = self.model_cls(train_mean, train_cov, **self.kwargs).fit()
        self.log_likelihood.append(np.log(results.likelihood(test_mean, test_cov)))
        self.squared_error.append(((results.params - test_mean) ** 2).mean())
        self.abs_error.append(abs(results.params - test_mean).mean())


def cross_validate(datafile: str) -> pd.DataFrame:
    """Perform cross validation for a given dataset.

    Args:
        datafile (str): Name of the data file.

    Returns:
        pd.DataFrame: Cross validation results.
    """
    df = pd.read_csv(os.path.join(PROCESSED_DATA_DIR, datafile))
    estimators = {
        "conventional": Estimator(Improper),
        "normal": Estimator(Normal),
        "james-stein": Estimator(Normal, fit_method="james_stein"),
        "bock": Estimator(Normal, fit_method="bock"),
        "nonparametric": Estimator(Nonparametric),
    }
    if datafile in ("penn.csv", "walmart.csv"):
        estimators["betabinom"] = Estimator(BetaBinomial)
        df = df[df.arm != CONTROL_ARM]

    # stratify by treatment (arm), group by participant
    kfold = StratifiedKFold(2, shuffle=True)
    compute_estimates = COMPUTE_ESTIMATES[datafile]
    df_participant = df.drop_duplicates("participant_id")
    index0, index1 = list(kfold.split(df_participant, df_participant.arm))[0]
    participant_id0 = df_participant.participant_id.iloc[index0]
    participant_id1 = df_participant.participant_id.iloc[index1]
    mean0, cov0 = compute_estimates(df[df.participant_id.isin(participant_id0)])
    mean1, cov1 = compute_estimates(df[df.participant_id.isin(participant_id1)])
    indices = np.where(mean0.index != CONTROL_ARM)[0]
    mean0, cov0 = mean0.iloc[indices], cov0[indices][:, indices]
    mean1, cov1 = mean1.iloc[indices], cov1[indices][:, indices]
    for estimator in estimators.values():
        estimator.update(mean0, cov0, mean1, cov1)
        estimator.update(mean1, cov1, mean0, cov0)
    return pd.concat(
        [
            pd.DataFrame(
                {
                    "estimator": name,
                    "fold": [0, 1],
                    "log_likelihood": estimator.log_likelihood,
                    "squared_error": estimator.squared_error,
                    "abs_error": estimator.abs_error,
                }
            )
            for name, estimator in estimators.items()
        ]
    )


if __name__ == "__main__":
    sim_no, datafile = sys.argv[1:]
    logging.info(f"Running simulation {sim_no} with datafile {datafile}.")
    sim_no = int(sim_no)
    np.random.seed(sim_no)
    df = cross_validate(datafile)
    df["sim_no"] = sim_no
    df["study"] = datafile_stub = datafile[: -len(".csv")]
    if not os.path.exists(RESULTS_DIR):
        os.mkdir(RESULTS_DIR)
    df.to_csv(
        os.path.join(RESULTS_DIR, f"sim_no={sim_no}-datafile={datafile_stub}.csv"),
        index=False,
    )
