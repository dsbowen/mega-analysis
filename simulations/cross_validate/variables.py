from itertools import product

N_REPEATS = 50
DATAFILE = "exercise.csv", "penn.csv", "walmart.csv"

variables = product(range(N_REPEATS), DATAFILE)
