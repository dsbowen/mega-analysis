# Megastudy Analysis

[![pipeline status](https://gitlab.com/dsbowen/mega-analysis/badges/master/pipeline.svg)](https://gitlab.com/dsbowen/mega-analysis/-/commits/master)
[![License](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://gitlab.com/dsbowen/mega-analysis/-/blob/master/LICENSE)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fmega-analysis/HEAD?urlpath=lab/tree/examples)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Code available [here](https://gitlab.com/dsbowen/mega-analysis). Data available [here](https://osf.io/w5nqm/). Please note that the OSF repository is for data only and does not contain the code files necessary to analyze them.

## Environment

Click [here](https://gitpod.io/#https://gitlab.com/dsbowen/mega-analysis/-/tree/master/) to run a pre-configured cloud environment with Gitpod.

If working locally, you'll need python-3.8 or higher. Set up your environment with:

```
$ pip install -r requirements.txt
```

## Data

Download the data [here](https://osf.io/w5nqm/). Put the `data` folder in the root directory of this repository (i.e., in the same folder as this README file).

The cleaned data should already be in this folder. To re-run the cleaning code, use:

```
$ make data
```

This command runs the following files in `src/make_data`:

- `make_data.py` cleans data from previous megastudies
- `clean_experiment.py` cleans the data from our adaptive effort experiment
- `compute_conventional_estimates.py` computes the conventional estimates (usually by OLS) for previous megastudies

The resulting files will be in `data/conventional_estimates` and `data/processed`.

## Main analyses

Run the main analyses with:

```
$ make analyze
```

This command runs the following files in `src/analyze`:

- `ax_simulations.py` analyzes the adaptive experimentation simulations using data from previous megastudies
- `coin_flip.py` simulates the coin-flipping megastudy
- `compare_models.py` compares OLS, Bayesian, hybrid, and projection models for previous megastudies
- `cross_validate.py` performs cross-validation to compare OLS, Bayesian, and hybrid models
- `experiment.py` analyzes our adaptive effort experiment

The resulting analyses will be in `results/analysis`.

## Data sources for previous megastudies

[Data](https://osf.io/rn8tw/?view_only=546ed2d8473f4978b95948a52712a3c5) for the Walmart flu megastudy dataset. The relevant files are "james_stein/dec_estsmat_js.csv.xlsx" and "james_stein/dec_vcov_js.csv" [here](https://osf.io/k8cy5/?view_only=546ed2d8473f4978b95948a52712a3c5) and "Code & Data/Data files/aggregate_data.xlsx", "Code & Data/Data files/WMT_Attributes_Analysis_Data1and3mo.csv", and "Code & Data/Data files/WMT_PI_Clean.csv" [here](https://osf.io/n8su7/?view_only=546ed2d8473f4978b95948a52712a3c5). Put these in `data/external/walmart`. Make sure to UTF-8 encode these files. 

[Summary statistics](https://osf.io/zqbg2/?view_only=c491df37a33840abbdedda4e60176f34) and [aggregate data](https://osf.io/r385j?view_only=c491df37a33840abbdedda4e60176f34) for the Penn-Geisinger flu megastudy dataset. Put these in `data/external/penn`.

[Data](https://osf.io/9s6mc/?view_only=8bb9282111c24f81a19c2237e7d7eba3) for the exercise study. Put `Data/SetupUp Data/pptdata.csv` in `data/external/exercise` before running the data cleaning scripts.

## Simulations

Make sure you have data from the megastudies in `data/processed`.

See `simulations/README.md` for instructions to re-run the adaptive experimentation simulations.