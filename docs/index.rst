.. Megastudy Analysis documentation master file, created by
   sphinx-quickstart on Mon Nov 12 14:17:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Megastudy Analysis documentation
===========================================

Data analysis for megastudies project

.. image:: https://gitlab.com/dsbowen/mega-analysis/badges/master/pipeline.svg
   :target: https://gitlab.com/dsbowen/mega-analysis/-/commits/master
.. image:: https://gitlab.com/dsbowen/mega-analysis/badges/master/coverage.svg
   :target: https://gitlab.com/dsbowen/mega-analysis/-/commits/master
.. image:: https://badge.fury.io/py/my-pypi-package-name.svg
   :target: https://badge.fury.io/py/my-pypi-package-name
.. image:: https://img.shields.io/badge/License-MIT-brightgreen.svg
   :target: https://gitlab.com/dsbowen/mega-analysis/-/blob/master/LICENSE
.. image:: https://mybinder.org/badge_logo.svg
   :target: https://mybinder.org/v2/gl/dsbowen%2Fmega-analysis/HEAD?urlpath=lab
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

|
Installation
============

.. code-block:: console

   $ pip install my-pypi-package-name

Quickstart
==========

TODO

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   my-pypi-package-name/index
   Changelog <changelog>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Citations
=========

.. code-block::

   
   @software(bowen2022my-pypi-package-name,
      title={ Megastudy Analysis },
      author={ Bowen, Dillon },
      year={ 2022 },
      url={ https://dsbowen.gitlab.io/mega-analysis }
   )

Acknowledgements
================

TODO